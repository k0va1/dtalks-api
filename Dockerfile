FROM ruby:2.4.1

RUN apt-get update -qqy && apt-get install  -qqyy postgresql postgresql-contrib libpq-dev cmake

RUN rm -rf /var/lib/apt/lists/*
