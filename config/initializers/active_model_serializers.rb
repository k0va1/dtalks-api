# frozen_string_literal: true

# Extended from Json adapter. Pass `success` option at the root layer
module ActiveModelSerializers
  module Adapter
    class DtalksAdapter < Json
      def serializable_hash(options = nil)
        options = serialization_options(options)
        serialized_hash = {
          success: options[:success],
          root => Attributes.new(serializer, instance_options).serializable_hash(options)
        }
        serialized_hash[meta_key] = meta unless meta.blank?

        self.class.transform_key_casing!(serialized_hash, instance_options)
      end
    end
  end
end

ActiveModelSerializers.config.adapter = :dtalks_adapter
