module RedisStorage
  class << self
    def redis
      @redis ||= Redis.new(
        host: ENV['REDIS_HOST'],
        port: ENV['REDIS_PORT'],
        db:   ENV['REDIS_DB']
      )
    end

    def appearance
      @appearance ||= Redis::Namespace.new(:appearance, redis: redis)
    end
  end
end

# To clear out the db before each test
RedisStorage.redis.flushdb if Rails.env == 'test'
