# frozen_string_literal: true

FactoryBot.define do
  factory :photo do
    image 'MyText'
    company nil
  end
end
