# frozen_string_literal: true

FactoryBot.define do
  factory :user, aliases: [:author, :owner] do
    sequence(:email) { |n| "#{n}#{Faker::Internet.email}" }
    sequence(:username) { |n| "#{Faker::Internet.user_name}#{n}" }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    password '123123123'
    password_confirmation '123123123'
    avatar { File.open('spec/support/test_files/valid_avatar.jpg') }

    trait :admin do
      after(:build) do |user|
        user.add_role(:admin)
      end
    end

    after(:build) do |user|
      user.add_role(:user)
    end
  end
end
