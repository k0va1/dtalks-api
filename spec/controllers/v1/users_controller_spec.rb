# frozen_string_literal: true

RSpec.describe V1::UsersController, type: :controller do
  describe 'GET #show' do
    it_behaves_like 'a show action' do
      let(:entity) { create(:user) }
      let(:schema) { 'api/v1/users/show' }
    end
  end

  describe 'GET #index' do
    subject { get :index }

    context 'when user authenticated' do
      context 'when user is an admin' do
        let(:user) { create(:user, :admin) }

        before { authenticate_user(user) }

        it_behaves_like 'a success request'
        it { is_expected.to match_json_schema('api/v1/users/index') }
      end

      context 'when user is not an admin' do
        let(:user) { create(:user) }

        before { authenticate_user(user) }

        it 'returns unauthorized error' do
          is_expected.to have_http_status(:forbidden)
        end
      end
    end

    context 'when user does not authenticated' do
      let(:user) { create(:user, :admin) }

      it { is_expected.to have_http_status(:unauthorized) }
    end
  end
end
