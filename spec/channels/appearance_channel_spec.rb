# frozen_string_literal: true

# TODO: write more specs
RSpec.describe AppearanceChannel, type: :channel do
  it 'successfully subscribes' do
    stub_connection current_user: double('user', id: 123)
    subscribe
    expect(subscription).to be_confirmed
  end
end
