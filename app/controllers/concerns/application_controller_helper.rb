# frozen_string_literal: true

module ApplicationControllerHelper
  def render_success(data: {}, success: true, status: :ok)
    render json: data, success: success, status: status
  end
end
