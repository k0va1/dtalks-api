# frozen_string_literal: true

class V1::Topics::CommentsController < V1::CommentsController
  def commentable
    @commentable ||= Topic.find(params[:topic_id])
  end
end
