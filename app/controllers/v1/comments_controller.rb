# frozen_string_literal: true

# TODO: dry it later
class V1::CommentsController < ApplicationController
  before_action :authenticate_v1_user!, except: [:index]

  def create
    comment = Comment.new(comment_params)
    comment.commentable = commentable
    comment.author = current_v1_user
    authorize comment

    comment.save!
    render json: comment, success: true, status: :created, serializer: V1::CommentSerializer
  end

  def update
    comment = comment(params[:id])
    authorize comment
    comment.update!(comment_params)
    render json: comment, success: true, status: :ok, serializer: V1::CommentSerializer
  end

  def destroy
    comment = comment(params[:id])
    authorize comment
    comment.destroy!
    render json: comment, success: true, status: :ok, serializer: V1::CommentSerializer
  end

  def index
    comments = commentable.comments.includes(:author)
    render json: comments, success: true, status: :ok, each_serializer: V1::CommentSerializer
  end

  private

  def comment_params
    params.permit(:text)
  end

  def comment(id)
    Comment.find(id)
  end
end
