# frozen_string_literal: true

module V1
  module Users
    class SessionsController < DeviseTokenAuth::SessionsController
      def create
        super do |user|
          V1::Users::AppearanceService.new(user).appear
        end
      end

      def destroy
        super do |user|
          V1::Users::AppearanceService.new(user).away
        end
      end

      def render_create_success
        user = @resource
        render json: user, success: true, serializer: V1::UserSerializer, status: :created
      end
    end
  end
end
