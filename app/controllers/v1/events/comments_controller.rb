# frozen_string_literal: true

class V1::Events::CommentsController < V1::CommentsController
  def commentable
    @commentable ||= Event.find(params[:event_id])
  end
end
