# frozen_string_literal: true

module V1
  class CompaniesController < ApplicationController
    before_action :authenticate_v1_user!, except: [:show, :index]

    def index
      companies = Company.search(params[:q]).order_by(params[:order_by]).page(params[:page])
      render json: companies, success: true, root: :data, status: :ok
    end

    def show
      company = company(params[:id])
      render json: company, success: true, status: :ok
    end

    def update
      company = company(params[:id])
      authorize company
      company.update!(company_params)
      render json: company, success: true, status: :ok
    end

    def create
      company = Company.new(company_params)
      company.owner = current_v1_user
      authorize company
      company.save!
      render json: company, success: true, status: :created
    end

    def destroy
      company = company(params[:id])
      authorize company
      company.destroy!
      render json: company, success: true, status: :ok
    end

    private

    def company(id)
      Company.find(id)
    end

    def company_params
      params.permit(:name, :city, :info, :logo_data_uri, :url, photos_attributes: [:image_data_uri])
    end
  end
end
