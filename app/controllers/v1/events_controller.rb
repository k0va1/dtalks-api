# frozen_string_literal: true

module V1
  class EventsController < ApplicationController
    before_action :authenticate_v1_user!, except: [:show, :index]
    impressionist action: [:show]

    def show
      event = event(params[:id])
      render json: event, success: true, status: :ok
    end

    # TODO: extract to query
    def index
      events = if params[:status].present?
                 Event.past.includes([:author, :participants])
               else
                 Event.upcoming.includes([:author, :participants])
               end
      events = events.city_events(params[:cities]) if params[:cities].present?
      # events = Event.group_by_day(events)
      # events = events.merge(success: true) if events.blank?
      render json: events, success: true, status: :ok, root: :data
    end

    def update
      event = event(params[:id])
      authorize event
      event.update!(event_params)
      render json: event, success: true, status: :ok
    end

    def destroy
      event = event(params[:id])
      authorize event
      event.destroy!
      render json: { success: true }, status: :ok, root: :data
    end

    def create
      event = Event.new(event_params)
      authorize event
      event.author = current_v1_user
      event.participants.build(user: current_v1_user, event: event)
      event.save!
      render json: event, success: true, status: :created
    end

    private

    def event_params
      params.permit(:id, :title, :text, :start_time, :end_time, :city, :user_id, :photo, :online, :address, :latitude,
                    :longitude, :price, :free)
    end

    def event(id)
      Event.find(id)
    end
  end
end
