# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    def disconnect
      ActionCable.server.broadcast(
        'notifications',
        type: 'alert', data: "#{current_user} disconnected"
      )
    end

    private

    def find_verified_user
      cook = JSON.parse(cookies['vuex'])['authHeaders']
      access_token = cook['access-token']
      client = cook['client']
      uid = cook['uid']

      current_user = User.find_by(uid: uid)

      current_user if current_user && current_user.valid_token?(access_token, client)
    end
  end
end
