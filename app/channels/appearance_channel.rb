# frozen_string_literal: true

class AppearanceChannel < ApplicationCable::Channel
  CHANNEL_NAME = 'appearance_channel'

  def subscribed
    V1::Users::AppearanceService.new(current_user).appear if current_user
    stream_from(CHANNEL_NAME)
  end

  def unsubscribed
    V1::Users::AppearanceService.new(current_user).away if current_user
  end
end
