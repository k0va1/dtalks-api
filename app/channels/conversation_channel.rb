# frozen_string_literal: true

class ConversationChannel < ApplicationCable::Channel
  def subscribed
    puts 'subscribed'
    stream_from "my_channel_#{current_user.id}"
  end
end
