# frozen_string_literal: true

module V1
  module Users
    class AppearanceService
      attr_reader :user

      def initialize(user)
        @user = user
      end

      def appear
        RedisStorage.appearance.set(user.id, 'online')
        broadcast
      end

      def away
        RedisStorage.appearance.del(user.id)
        broadcast
      end

      private

      def broadcast
        online_users_ids = RedisStorage.appearance.keys('*').map(&:to_i)
        ActionCable.server.broadcast(AppearanceChannel::CHANNEL_NAME, online_users_ids)
      end
    end
  end
end
