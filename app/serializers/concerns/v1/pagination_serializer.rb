# frozen_string_literal: true

module V1
  module PaginationSerializer
    extend ActiveSupport::Concern

    included do
      attributes :current_page, :total_pages
    end
  end
end
