# frozen_string_literal: true

module V1
  class PhotoSerializer < ActiveModel::Serializer
    attributes :url

    def url
      object[:original].url
    end
  end
end
