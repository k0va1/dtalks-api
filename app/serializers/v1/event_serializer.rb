# frozen_string_literal: true

module V1
  class EventSerializer < BaseSerializer
    attributes :title, :text, :start_time, :end_time, :online, :participants_count, :free, :created_at
    # attributes :city, :address, :longitude, :latitude, unless: :online?
    attribute :free, unless: :free?
    attribute :impressions_count, key: :views_count

    has_one :author, serializer: AuthorSerializer
    has_many :participants
    has_many :comments

    def free?
      object.free?
    end

    def online?
      object.online?
    end
  end
end
