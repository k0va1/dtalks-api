# frozen_string_literal: true

module V1
  class UserProfileSerializer < UserSerializer
    has_one :company
    has_many :topics
    has_many :comments
    has_many :events
    has_many :links
  end
end
