# frozen_string_literal: true

module V1
  class AuthorSerializer < BaseSerializer
    attributes :full_name, :username, :position
    has_one :avatar, serializer: AvatarSerializer
  end
end
