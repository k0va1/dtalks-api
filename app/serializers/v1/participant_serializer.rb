# frozen_string_literal: true

module V1
  class ParticipantSerializer < ActiveModel::Serializer
    attributes :username, :first_name, :last_name, :full_name, :position, :avatar

    %i[username first_name last_name full_name position avatar].each do |attr|
      define_method(attr) { object.user.send(attr) }
    end
  end
end
