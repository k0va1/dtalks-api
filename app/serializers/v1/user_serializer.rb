# frozen_string_literal: true

module V1
  class UserSerializer < BaseSerializer
    attributes :username, :email, :first_name, :last_name, :full_name, :position, :bio, :city

    has_one :avatar, serializer: V1::AvatarSerializer
  end
end
