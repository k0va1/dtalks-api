# frozen_string_literal: true

module V1
  class CommentSerializer < BaseSerializer
    attributes :text, :created_at, :updated_at

    belongs_to :author
  end
end
