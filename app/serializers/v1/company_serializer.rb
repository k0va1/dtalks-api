# frozen_string_literal: true

module V1
  class CompanySerializer < BaseSerializer
    # include V1::PaginationSerializer
    attributes :name, :city, :info, :url, :employees_count, :vacancies_count, :reviews_count, :rating, :created_at

    # has_one :logo
    # has_many :photos
  end
end
