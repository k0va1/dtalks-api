# frozen_string_literal: true

module V1
  class AvatarSerializer < ActiveModel::Serializer
    attributes :original, :thumbnail

    def original
      { url: object[:original].url }
    end

    def thumbnail
      { url: object[:thumbnail].url }
    end
  end
end
