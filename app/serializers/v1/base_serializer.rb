# frozen_string_literal: true

module V1
  class BaseSerializer < ActiveModel::Serializer
    type :data

    attribute :id
  end
end
