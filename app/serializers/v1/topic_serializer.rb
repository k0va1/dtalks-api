# frozen_string_literal: true

module V1
  class TopicSerializer < BaseSerializer
    attributes :title, :text, :comments_count, :created_at, :updated_at
    attribute :impressions_count, key: :views_count
    attribute :tag_list, key: :tags

    has_one :author, serializer: AuthorSerializer
    has_many :comments
  end
end
