# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  attr_reader :user

  def initialize(user, _)
    @user = user
  end

  def index?
    user.has_role?(:admin)
  end

  def create?
    true
  end

  def update?
    user.has_role?(:user)
  end

  def destroy?
    user.has_role?(:user)
  end
end
