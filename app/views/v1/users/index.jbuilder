# frozen_string_literal: true

json.success true

json.data do
  json.users @users, partial: 'v1/users/user', as: :user
end
